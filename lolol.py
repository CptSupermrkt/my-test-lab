#!/usr/bin/python

import time

def print_joke():
    print "Why do chicken coops only have two doors?"
    time.sleep(3)
    print "Because if they had four, they would be chicken sedans!"

print "Welcome to my test program!"
print "Here's a joke"
print_joke()
